import unittest
from unittest import result
import simple_math

class TestSimpleMath(unittest.TestCase):

    def test_add(self):
        result = simple_math.add(10, 5)
        self.assertEqual(result, 15)

    def test_subtract(self):
        result = simple_math.subtract(10, 5)
        self.assertEqual(result, 5)

if __name__ == '__main__':
    unittest.main()