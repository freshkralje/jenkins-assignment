def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def main():
    print("10 + 5: " + str(add(10, 5)))
    print("10 - 5: " + str(subtract(10, 5)))

    with open("file.txt","w") as f:
        f.write("Simple math calculations are done!")


if __name__ == '__main__':
    main()
